package net.brinkervii;

import net.brinkervii.rockit.alchemy.cli.CLIExtract;
import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(
		name = "alchemy",
		description = "Roblox file manipulation tool and library",
		subcommands = {CLIExtract.class}
)
public class AlchemyCLI implements Runnable {
	public static void main(String[] arguments) {
		CommandLine.run(new AlchemyCLI(), arguments);
	}

	@Override
	public void run() {
		CommandLine.usage(this, System.out);
	}
}
