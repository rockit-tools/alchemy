package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;

@Data
public final class RbxFloat extends DataType {
	private Float value;

	public RbxFloat(Float value) {
		this.value = value;
	}
}
