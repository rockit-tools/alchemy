package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public final class Vector2 extends DataType {
	private Float x;
	private Float y;

	public Vector2(Float x, Float y) {
		this.x = x;
		this.y = y;
	}
}
