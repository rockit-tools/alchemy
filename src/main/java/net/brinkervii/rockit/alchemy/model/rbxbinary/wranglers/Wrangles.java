package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Wrangles {
	RbxBinaryDataType value();
}
