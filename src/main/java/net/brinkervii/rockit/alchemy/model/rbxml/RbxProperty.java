package net.brinkervii.rockit.alchemy.model.rbxml;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.util.HashMap;

@Data
@Slf4j
@JsonSerialize
public class RbxProperty {
	private boolean complex;
	private String type;
	private String name = "Unnamed";
	private String value = "";
	private HashMap<String, String> attributes = new HashMap<>();

	public RbxProperty(Element element) {
		this.type = element.tagName();

		if (element.hasAttr("name")) {
			this.name = element.attr("name").trim();
		} else {
			log.warn("Element has no name");
		}

		this.complex = element.children().size() > 0;
		if (complex) {
			element.children().forEach(elem -> attributes.put(elem.tagName(), elem.ownText().trim()));
		} else {
			this.value = element.ownText().trim();
		}
	}
}
