package net.brinkervii.rockit.alchemy.model.rbxbinary.generator;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxMagic;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryFooter;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryMember;

import java.io.IOException;

@RbxMagic("END")
public final class RbxBinaryFooterGenerator extends RbxBinaryMemberGenerator {
	@Override
	public RbxBinaryMember generateMember(RobloxBinaryInputStream in) throws IOException {
		return RbxBinaryFooter.fromStream(in);
	}
}
