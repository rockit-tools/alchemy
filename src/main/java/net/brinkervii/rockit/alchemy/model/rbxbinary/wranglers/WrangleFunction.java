package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

public interface WrangleFunction<T> {
	T wrangle(int index) throws Exception;
}
