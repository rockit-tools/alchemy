package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.model.rbx.data.DataType;
import net.brinkervii.rockit.model.rbx.data.RbxString;

@Slf4j
@Wrangles(RbxBinaryDataType.String)
public final class RbxStringWrangler extends RbxBinaryDataWrangler {
	@Override
	public DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException {
		return read(new RbxString[instanceRecord.getNumberOfInstances()], (int index) -> new RbxString(in.readRbxString()));
	}
}
