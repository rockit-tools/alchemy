package net.brinkervii.rockit.alchemy.model.rbxbinary.generator;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryMember;

import java.io.IOException;

public abstract class RbxBinaryMemberGenerator {
	public abstract RbxBinaryMember generateMember(final RobloxBinaryInputStream in) throws IOException;
}
