package net.brinkervii.rockit.alchemy.model.rbxbinary.generator;

import lombok.Getter;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;

public final class RbxBinaryHeader {
	private final static int SIGNATURE_LENGTH = 16;
	private final static int RESERVED_DATA_LENGTH = 8;

	@Getter
	private byte[] signature = new byte[SIGNATURE_LENGTH];
	@Getter
	private int numberOfTypes = 0;
	@Getter
	private int numberOfObjects = 0;
	@Getter
	private byte[] reserved = new byte[RESERVED_DATA_LENGTH];

	public static RbxBinaryHeader fromStream(RobloxBinaryInputStream in) throws IOException {
		final RbxBinaryHeader header = new RbxBinaryHeader();

		in.readNBytes(header.signature, 0, SIGNATURE_LENGTH);
		header.numberOfTypes = in.readIntLe();
		header.numberOfObjects = in.readIntLe();
		in.readNBytes(header.reserved, 0, RESERVED_DATA_LENGTH);

		return header;
	}

}
