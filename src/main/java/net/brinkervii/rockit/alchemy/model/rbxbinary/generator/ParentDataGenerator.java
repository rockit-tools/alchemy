package net.brinkervii.rockit.alchemy.model.rbxbinary.generator;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.ParentData;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryMember;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxMagic;

import java.io.IOException;

@RbxMagic("PRNT")
public final class ParentDataGenerator extends RbxBinaryMemberGenerator {
	@Override
	public RbxBinaryMember generateMember(RobloxBinaryInputStream in) throws IOException {
		return ParentData.fromStream(in);
	}
}
