package net.brinkervii.rockit.alchemy.model.rbxbinary.generator;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryMember;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxMagic;

import java.io.IOException;

@RbxMagic("INST")
public final class InstanceRecordGenerator extends RbxBinaryMemberGenerator {
	@Override
	public RbxBinaryMember generateMember(RobloxBinaryInputStream in) throws IOException {
		return InstanceRecord.fromStream(in);
	}
}
