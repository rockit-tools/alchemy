package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

public class WranglingException extends Exception {
	public WranglingException(Exception e) {
		super(e);
	}
}
