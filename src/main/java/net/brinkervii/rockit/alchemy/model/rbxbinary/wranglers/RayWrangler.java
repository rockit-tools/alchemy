package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.model.rbx.data.DataType;
import net.brinkervii.rockit.model.rbx.data.Ray;
import net.brinkervii.rockit.model.rbx.data.Vector3;

@Slf4j
@Wrangles(RbxBinaryDataType.Ray)
public final class RayWrangler extends RbxBinaryDataWrangler {
	@Override
	public DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException {
		return read(new Ray[instanceRecord.getNumberOfInstances()], index -> {
			final var origin = new Vector3(
					in.readFloat(),
					in.readFloat(),
					in.readFloat()
			);

			final var direction = new Vector3(
					in.readFloat(),
					in.readFloat(),
					in.readFloat()
			);

			return new Ray(origin, direction);
		});
	}
}
