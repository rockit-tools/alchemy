package net.brinkervii.rockit.alchemy.model.rbxbinary.structure;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;
import java.util.ArrayList;

public final class ReferentArray extends ArrayList<Integer> {
	public ReferentArray() {

	}

	private ReferentArray(InterleavedArray array) {
		int accumulator = 0;

		for (int i = 0; i < array.getSize(); i++) {
			if (i == 0) {
				accumulator = array.getInt(i);
				add(accumulator);
			} else {
				accumulator = accumulator + array.getInt(i);
				add(accumulator);
			}
		}
	}

	public static ReferentArray fromStream(RobloxBinaryInputStream in, int instanceCount) throws IOException {
		return new ReferentArray(InterleavedArray.fromStream(in, instanceCount, 4));
	}
}
