package net.brinkervii.rockit.alchemy.model.rbxbinary;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.PropertyRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryFooter;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryMembers;
import net.brinkervii.rockit.alchemy.model.rbxbinary.exception.NoMagicException;
import net.brinkervii.rockit.alchemy.model.rbxbinary.generator.*;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxBlob;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxCompressionHeader;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxMagic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Slf4j
public final class RbxBinaryModel {
	private final static HashMap<String, RbxBinaryMemberGenerator> memberGenerators = new HashMap<>();

	static {
		addMemberGenerator(new SharedStringsGenerator());
		addMemberGenerator(new InstanceRecordGenerator());
		addMemberGenerator(new PropertyRecordGenerator());
		addMemberGenerator(new RbxBinaryFooterGenerator());
		addMemberGenerator(new ParentDataGenerator());
	}

	private static void addMemberGenerator(RbxBinaryMemberGenerator generator) {
		final RbxMagic annotation = generator.getClass().getAnnotation(RbxMagic.class);
		if (annotation != null) {
			memberGenerators.put(annotation.value(), generator);
		} else {
			log.error("No magic for generator " + generator);
		}
	}

	private RbxBinaryMembers members = new RbxBinaryMembers();
	private RbxBinaryHeader header = null;

	public static RbxBinaryModel load(InputStream inputStream) throws IOException {
		final RobloxBinaryInputStream in = new RobloxBinaryInputStream(inputStream);
		final RbxBinaryModel model = new RbxBinaryModel();

		model.header = RbxBinaryHeader.fromStream(in);
		final AtomicBoolean endOfFile = new AtomicBoolean(false);

		while (!endOfFile.get()) {
			final RbxCompressionHeader compressionHeader = RbxCompressionHeader.fromStream(in);
			if (!memberGenerators.containsKey(compressionHeader.getMagic())) {
				log.error("No member generator for magic key " + compressionHeader.getMagic());
				throw new NoMagicException(compressionHeader.getMagic());
			}

			final var blob = RbxBlob.fromStream(in, compressionHeader);
			final var generator = memberGenerators.get(compressionHeader.getMagic());
			final var generatedMember = generator.generateMember(blob.getStream());
			model.members.add(Objects.requireNonNull(generatedMember));

			endOfFile.set(model.members.get(model.members.size() - 1) instanceof RbxBinaryFooter);
		}

		model.interpretProperties();

		return model;
	}

	private void interpretProperties() {
		final Map<Integer, InstanceRecord> instanceRecords = members.parallelStream()
				.filter(member -> member instanceof InstanceRecord)
				.map(member -> (InstanceRecord) member)
				.collect(Collectors.toUnmodifiableMap(InstanceRecord::getTypeId, v -> v));

		final List<PropertyRecord> propertyRecords = members.parallelStream()
				.filter(member -> member instanceof PropertyRecord)
				.map(member -> (PropertyRecord) member)
				.collect(Collectors.toUnmodifiableList());

		propertyRecords.forEach(record -> record.interpretData(instanceRecords.get(record.getTypeId())));
	}

	public static RbxBinaryModel load(File inputFile) throws IOException {
		return load(new FileInputStream(inputFile));
	}
}
