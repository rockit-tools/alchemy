package net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember;

import lombok.Getter;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;

public final class SharedStringsMetadata {
	@Getter
	private int version = 0;
	@Getter
	private int count = 0;

	public static SharedStringsMetadata fromStream(RobloxBinaryInputStream in) throws IOException {
		final SharedStringsMetadata metadata = new SharedStringsMetadata();

		metadata.version = in.readIntLe();
		metadata.count = in.readIntLe();

		return metadata;
	}
}
