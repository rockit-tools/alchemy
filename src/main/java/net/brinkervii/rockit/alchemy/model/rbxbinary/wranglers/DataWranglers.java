package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;

import java.util.HashMap;

@Slf4j
public final class DataWranglers extends HashMap<RbxBinaryDataType, RbxBinaryDataWrangler> {
	public DataWranglers() {
		add(new AxesWrangler());
		add(new BooleanWrangler());
		add(new BrickColorWrangler());
		add(new CFrameWrangler());
		add(new Color3Wrangler());
		add(new ColorSequenceKeypointWrangler());
		add(new ColorSequenceWrangler());
		add(new FacesWrangler());
		add(new NumberRangeWrangler());
		add(new NumberSequenceKeypointWrangler());
		add(new NumberRangeWrangler());
		add(new PhysicalPropertiesWrangler());
		add(new RayWrangler());
		add(new RbxStringWrangler());
		add(new RectWrangler());
		add(new Region3Int16Wrangler());
		add(new Region3Wrangler());
		add(new UDim2Wrangler());
		add(new UDimWrangler());
		add(new Vector2Wrangler());
		add(new Vector3Int16Wrangler());
		add(new Vector3Wrangler());
		add(new RbxIntegerWrangler());
		add(new RbxFloatWrangler());
	}

	private void add(RbxBinaryDataWrangler wrangler) {
		final Class<? extends RbxBinaryDataWrangler> clazz = wrangler.getClass();
		final Wrangles annotation = clazz.getAnnotation(Wrangles.class);
		if (annotation != null) {
			put(annotation.value(), wrangler);
		} else {
			log.warn(String.format("Wrangler %s has no annotation, it will be ignored", clazz.getSimpleName()));
		}
	}
}
