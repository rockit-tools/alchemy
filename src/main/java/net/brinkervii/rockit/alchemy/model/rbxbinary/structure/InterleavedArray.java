package net.brinkervii.rockit.alchemy.model.rbxbinary.structure;

import lombok.Getter;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;
import java.nio.ByteBuffer;

public class InterleavedArray {
	@Getter
	private final int size;
	private final int typeSize;
	private byte[] data = new byte[0];

	public InterleavedArray(int size, int typeSize) {
		this.size = size;
		this.typeSize = typeSize;
	}

	public byte[] get(int index) {
		final byte[] d = new byte[typeSize];
		System.arraycopy(data, index * typeSize, d, 0, typeSize);

		return d;
	}

	public int getInt(int index) {
		final int rawInt = ByteBuffer.wrap(get(index)).getInt();

		final boolean negative = rawInt % 2 != 0;
		if (negative) {
			return (rawInt + 1) / -2;
		} else {
			return rawInt / 2;
		}
	}

	public float getFloat(int index) {
		return ByteBuffer.wrap(get(index)).getFloat();
	}

	public static InterleavedArray fromStream(RobloxBinaryInputStream in, int size, int typeSize) throws IOException {
		final InterleavedArray array = new InterleavedArray(size, typeSize);

		final int sz = size * typeSize;
		final byte[] interleaved = new byte[sz];
		in.readNBytes(interleaved, 0, sz);

		array.data = new byte[sz];

		for (int item_index = 0; item_index < size; item_index++) {
			for (int byte_index = 0; byte_index < typeSize; byte_index++) {
				final int deinterleavedPosition = item_index * typeSize + byte_index;
				final int interleavedPosition = byte_index * size + item_index;

				array.data[deinterleavedPosition] = interleaved[interleavedPosition];
			}
		}

		return array;
	}
}
