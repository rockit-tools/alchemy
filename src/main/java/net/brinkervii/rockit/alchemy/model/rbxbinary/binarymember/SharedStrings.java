package net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;
import java.util.ArrayList;

public final class SharedStrings extends RbxBinaryMember {
	private ArrayList<SharedString> strings = new ArrayList<>();

	public static SharedStrings fromStream(RobloxBinaryInputStream in, SharedStringsMetadata metadata) throws IOException {
		final SharedStrings sharedStrings = new SharedStrings();

		for (int id = 0; id < metadata.getCount(); id++) {
			sharedStrings.strings.add(SharedString.fromStream(in, id));
		}

		return sharedStrings;
	}

	public int size() {
		return strings.size();
	}

	public void add(SharedString sharedString) {
		strings.add(sharedString);
	}
}
