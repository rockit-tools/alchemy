package net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember;

import lombok.Getter;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;
import java.util.Base64;

public final class SharedString {
	private final static Base64.Encoder BASE64_ENCODER = Base64.getEncoder();
	private final static int KEY_LENGTH = 16;

	@Getter
	private int id;
	@Getter
	private String key = "undefined";
	@Getter
	private String value = "undefined";

	public SharedString(SharedStrings parent) {
		this.id = parent.size();
		parent.add(this);
	}

	public SharedString(int id) {
		this.id = id;
	}

	public static SharedString fromStream(RobloxBinaryInputStream in, int id) throws IOException {
		final SharedString sharedString = new SharedString(id);

		final byte[] keyBuffer = new byte[KEY_LENGTH];
		in.readNBytes(keyBuffer, 0, KEY_LENGTH);

		sharedString.key = BASE64_ENCODER.encodeToString(keyBuffer);
		sharedString.value = BASE64_ENCODER.encodeToString(in.readRbxStringRaw());

		return sharedString;
	}
}
