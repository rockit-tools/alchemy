package net.brinkervii.rockit.alchemy.model.rbxml;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.Stack;
import java.util.stream.Stream;

@Data
@Slf4j
@JsonSerialize
public class RbxModel {
	private ArrayList<RbxItem> items = new ArrayList<>();
	private ArrayList<RbxExternal> externals = new ArrayList<>();

	public RbxModel(Element element) {
		element.children().forEach(rootElement -> rootElement.children().forEach(elem -> {
			switch (elem.tagName()) {
				case "Item":
					items.add(new RbxItem(elem));
					break;

				case "External":
					externals.add(new RbxExternal(elem));
					break;
				default:
					log.warn("Unknown tag name " + elem.tagName());
					break;
			}
		}));
	}

	public Stream<RbxItem> flatMap(boolean parallel) {
		final ArrayList<RbxItem> flatMap = new ArrayList<>();

		final Stack<RbxItem> stack = new Stack<>();
		stack.addAll(items);

		while (stack.size() > 0) {
			final RbxItem top = stack.pop();

			flatMap.add(top);
			stack.addAll(top.getChildren());
		}

		if (parallel) {
			return flatMap.parallelStream();
		} else {
			return flatMap.stream();
		}
	}
}
