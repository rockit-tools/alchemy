package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.model.rbx.data.DataType;

public abstract class RbxBinaryDataWrangler {
	public abstract DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException;

	protected <PT> PT[] read(PT[] targetArray, WrangleFunction<PT> f) throws WranglingException {
		for (int i = 0; i < targetArray.length; i++) {
			try {
				targetArray[i] = f.wrangle(i);
			} catch (Exception e) {
				throw new WranglingException(e);
			}
		}

		return targetArray;
	}
}
