package net.brinkervii.rockit.alchemy.extract;

import java.io.IOException;

public abstract class FSTask {
	public abstract int priority();

	public abstract void run() throws IOException;
}
