package net.brinkervii.rockit.alchemy.extract;

import java.io.File;
import java.io.IOException;

public class DirectoryCreationException extends IOException {
	public DirectoryCreationException(File file) {
		super("Failed to create directory " + file);
	}
}
