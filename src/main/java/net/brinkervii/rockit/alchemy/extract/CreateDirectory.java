package net.brinkervii.rockit.alchemy.extract;

import java.io.File;
import java.io.IOException;

public class CreateDirectory extends FSTask {
	private final File file;

	public CreateDirectory(File file) {
		this.file = file;
	}

	@Override
	public int priority() {
		return 0;
	}

	@Override
	public void run() throws IOException {
		if (!file.exists()) {
			if (!file.mkdirs()) {
				throw new DirectoryCreationException(file);
			}
		}
	}
}
