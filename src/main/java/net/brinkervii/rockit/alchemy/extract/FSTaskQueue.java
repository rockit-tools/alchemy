package net.brinkervii.rockit.alchemy.extract;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedList;

@Slf4j
public class FSTaskQueue extends LinkedList<FSTask> implements Runnable {
	private Exception exception;

	@Override
	public void run() {
		final String className = getClass().getSimpleName();

		sort(Comparator.comparingInt(FSTask::priority));

		log.info(String.format("Running %s with %d items", className, size()));
		final long startTime = System.currentTimeMillis();

		while (!isEmpty()) {
			final FSTask pop = pop();
			try {
				pop.run();
			} catch (IOException e) {
				log.error(String.format("%s stopped early due to an error: %s", className, e.getMessage()));
				this.exception = e;
			}
		}
		
		final double timeInSeconds = (double) (System.currentTimeMillis() - startTime) / 1000d;
		log.info(String.format("%s completed in %f seconds", className, timeInSeconds));
	}

	@Override
	public boolean add(FSTask fsTask) {
		if (fsTask == null) {
			log.info("Added a null FSTask, that's illegal!");
		}

		return super.add(fsTask);
	}

	public void except() throws Exception {
		if (this.exception != null)
			throw exception;
	}
}
