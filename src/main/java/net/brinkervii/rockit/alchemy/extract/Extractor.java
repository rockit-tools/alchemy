package net.brinkervii.rockit.alchemy.extract;

import net.brinkervii.rockit.alchemy.task.ExtractRobloxFileTaskConfiguration;

public abstract class Extractor implements Runnable {
	protected final ExtractRobloxFileTaskConfiguration configuration;

	public Extractor(ExtractRobloxFileTaskConfiguration configuration) {

		this.configuration = configuration;
	}
}
