package net.brinkervii.rockit.alchemy.task;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.BinaryModelFileExtractor;
import net.brinkervii.rockit.alchemy.extract.Extractor;
import net.brinkervii.rockit.alchemy.extract.XMLModelFileExtractor;

import java.io.File;

@Slf4j
public class ExtractRobloxFileTask implements Runnable {
	private final ExtractRobloxFileTaskConfiguration configuration;

	public ExtractRobloxFileTask(ExtractRobloxFileTaskConfiguration configuration) {
		this.configuration = configuration;
	}

	@Override
	public void run() {
		final File inputFile = configuration.getInputFile();

		final String[] split = inputFile.toString().split("\\.");
		final String extension = split[split.length - 1].toLowerCase();

		Extractor extractor = null;

		switch (extension) {
			case "rbxmx":
			case "rbxlx":
				log.info("Extracting XML model file");
				extractor = new XMLModelFileExtractor(configuration);

				break;

			case "rbxl":
				log.info("Extracting binary place file");
				extractor = new BinaryModelFileExtractor(configuration);

				break;

			case "rbxm":
				log.error("Extracting binary model files is not supported");
				break;

			default:
				log.error("File " + inputFile + " is unsupported, not extracting.");
				break;
		}

		if (extractor != null)
			extractor.run();
	}
}
