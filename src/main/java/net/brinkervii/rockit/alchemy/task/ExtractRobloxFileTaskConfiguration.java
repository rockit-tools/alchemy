package net.brinkervii.rockit.alchemy.task;

import lombok.Getter;

import java.io.File;

public class ExtractRobloxFileTaskConfiguration {
	@Getter
	private File inputFile = null;
	@Getter
	private File outputDirectory = null;
	@Getter
	private boolean extractScripts = false;
	@Getter
	private boolean dumpInstances = false;

	public ExtractRobloxFileTaskConfiguration inputFile(File inputFile) throws InvalidConfigurationException {
		this.inputFile = inputFile;

		if (!inputFile.exists()) {
			throw new InvalidConfigurationException("The input file " + inputFile + " does not exist");
		}

		return this;
	}

	public ExtractRobloxFileTaskConfiguration outputDirectory(File outputDirectory) throws InvalidConfigurationException {
		this.outputDirectory = outputDirectory;

		if (!outputDirectory.exists()) {
			if (!outputDirectory.mkdirs()) {
				throw new InvalidConfigurationException("Could not create output directory " + outputDirectory);
			}
		} else {
			if (!outputDirectory.isDirectory()) {
				throw new InvalidConfigurationException("Output directory " + outputDirectory + " is not actually a directory.");
			}
		}

		return this;
	}

	public ExtractRobloxFileTaskConfiguration scripts(boolean scripts) {
		this.extractScripts = scripts;
		return this;
	}

	public ExtractRobloxFileTaskConfiguration instances(boolean instances) {
		this.dumpInstances = instances;
		return this;
	}
}
