package net.brinkervii.rockit.alchemy.task;

public class InvalidConfigurationException extends Exception {
	public InvalidConfigurationException(String s) {
		super(s);
	}
}
