package net.brinkervii.rockit.alchemy.cli;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.task.ExtractRobloxFileTask;
import net.brinkervii.rockit.alchemy.task.ExtractRobloxFileTaskConfiguration;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.util.concurrent.Callable;

@Slf4j
@Command(
		name = "extract",
		description = "Extract the contents of a Roblox place/model file"
)
public class CLIExtract implements Callable<Void> {
	@Option(names = {"-f", "--file"}, description = "The input/source file to extract from", required = true)
	private File inputFile = null;

	@Option(names = {"-d", "--directory"}, description = "The output directory for the extract task", required = true)
	private File outputDirectory = null;

	@Option(names = {"-s", "--scripts"}, description = "Extract scripts from the source file")
	private boolean scripts = false;

	@Option(names = {"-m", "--instances"}, description = "Dump instances to json files")
	private boolean instances = false;

	@Override
	public Void call() throws Exception {
		final ExtractRobloxFileTaskConfiguration config = new ExtractRobloxFileTaskConfiguration();
		config
				.inputFile(inputFile)
				.outputDirectory(outputDirectory)
				.scripts(scripts)
				.instances(instances);

		log.info("Going to run extractor on " + config.getInputFile());

		new ExtractRobloxFileTask(config).run();

		return null;
	}
}
